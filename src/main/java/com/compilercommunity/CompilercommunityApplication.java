package com.compilercommunity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompilercommunityApplication {

    public static void main(String[] args) {
        SpringApplication.run(CompilercommunityApplication.class, args);
    }

}

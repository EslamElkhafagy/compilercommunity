package com.compilercommunity.controller;

import com.compilercommunity.entity.About;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("")
    public ResponseEntity showHomePage(Model model){


        return new ResponseEntity("Welcome", HttpStatus.OK);


    }
}

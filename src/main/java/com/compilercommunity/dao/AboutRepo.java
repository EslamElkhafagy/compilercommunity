package com.compilercommunity.dao;


import com.compilercommunity.entity.About;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AboutRepo extends JpaRepository<About,Long> {


}

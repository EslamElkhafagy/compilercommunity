package com.compilercommunity.dao;

import com.compilercommunity.entity.CommunityDepart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunityDepartRepo extends JpaRepository<CommunityDepart,Long> {
}

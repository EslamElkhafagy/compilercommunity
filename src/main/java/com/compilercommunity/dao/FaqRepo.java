package com.compilercommunity.dao;

import com.compilercommunity.entity.Faq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaqRepo extends JpaRepository<Faq, Long> {
}

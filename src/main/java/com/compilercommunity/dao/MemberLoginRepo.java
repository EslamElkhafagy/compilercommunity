package com.compilercommunity.dao;

import com.compilercommunity.entity.MemberLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberLoginRepo extends JpaRepository<MemberLogin, Long> {
}

package com.compilercommunity.dao;

import com.compilercommunity.entity.PostCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCategoryRepo extends JpaRepository<PostCategory, Long> {
}

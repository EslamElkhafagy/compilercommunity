package com.compilercommunity.dao;

import com.compilercommunity.entity.RateAndFeedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateAndFeedbackRepo extends JpaRepository<RateAndFeedback, Long> {
}

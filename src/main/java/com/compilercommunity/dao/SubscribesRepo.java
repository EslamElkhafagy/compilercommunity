package com.compilercommunity.dao;

import com.compilercommunity.entity.Subscribes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscribesRepo extends JpaRepository<Subscribes, Long> {
}

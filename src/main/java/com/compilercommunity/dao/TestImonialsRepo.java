package com.compilercommunity.dao;

import com.compilercommunity.entity.TestImonials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestImonialsRepo extends JpaRepository<TestImonials,Long> {
}

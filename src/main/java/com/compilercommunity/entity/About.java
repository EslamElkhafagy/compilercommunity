package com.compilercommunity.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "about")
public class About {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String content;
    private String image;
    private String title;


}

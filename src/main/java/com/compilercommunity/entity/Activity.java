package com.compilercommunity.entity;


import com.compilercommunity.entity.Enums.ActivityCategory;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name ="activity" )
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String pic;
    private String url;
    private String urlForm;

    @Temporal(TemporalType.DATE)
    private Date time;

    private String location;

    @Column(columnDefinition = "TEXT")
    private String details;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private ActivityCategory activityCategory;


}

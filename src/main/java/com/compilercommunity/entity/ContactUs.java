package com.compilercommunity.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "contactus")
public class ContactUs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private  String name;
    private  String subjectTitle;
    private  String message;
    private  String email;

    private byte seen;



}

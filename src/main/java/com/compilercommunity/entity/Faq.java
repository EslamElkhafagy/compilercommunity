package com.compilercommunity.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "faq")
public class Faq {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String question;
    private String answer;


}

package com.compilercommunity.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "member")
@Data
public class Member {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @UniqueElements
    private String phone;
    @UniqueElements
    private String email;
    private String url;
    private String pic;
    private String bio;
    private String positionTitle;
    private Date joinDate;
    private Date endDate;
    private byte allowPic;// allow member image to visible in our website


    @ManyToOne
    @JoinColumn(name = "depart_id",referencedColumnName = "id")
    private CommunityDepart communityDepart;

    @OneToOne(mappedBy = "member")
    @JsonIgnore
    private MemberLogin memberLogin;

    @OneToMany(mappedBy = "sendermember")
    @JsonIgnore
    private List<Tasks> senderTasksList;

    @OneToMany(mappedBy = "recievermember")
    @JsonIgnore
    private List<Tasks> recieverTasksList;

    @OneToMany(mappedBy = "member")
    @JsonIgnore
   private List<Post> postList;



}

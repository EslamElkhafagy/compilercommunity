package com.compilercommunity.entity;

import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
public class MemberLogin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @UniqueElements
    private String username;

    private String password;


    @OneToOne
    @JoinColumn(name = "member_id",referencedColumnName = "id")
    private Member member;




}

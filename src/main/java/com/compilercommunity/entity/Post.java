package com.compilercommunity.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;

    @Column(columnDefinition = "TEXT")
    private String body;

    @DateTimeFormat(pattern = "yy-mmm-dd")
    private Date createdDate;
    private String img;


    @ManyToOne
    @JoinColumn(name = "member_id",referencedColumnName = "id")
    private Member member;


    @ManyToOne
    @JoinColumn(name = "postcategory_id",referencedColumnName = "id")
    private PostCategory postCategory;

}

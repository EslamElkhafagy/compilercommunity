package com.compilercommunity.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "rateandfeedback")
public class RateAndFeedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double rate;

    private String comment;

    @OneToOne
    @JoinColumn(name = "task_id",referencedColumnName = "id")
    private Tasks tasks;

}

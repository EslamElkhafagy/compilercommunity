package com.compilercommunity.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "sponsor")
public class Sponsor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String url;

    private String logo;


}

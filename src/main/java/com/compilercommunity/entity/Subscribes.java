package com.compilercommunity.entity;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "subscribes")
public class Subscribes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;



}

package com.compilercommunity.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "tasks")
public class Tasks {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String description;

    private String status;

    @DateTimeFormat(pattern = "dd/mmm/yy")
    private Date deadline;

    @DateTimeFormat(pattern = "dd/mmm/yy")
    private Date creation;

    @ManyToOne
    @JoinColumn(name = "sendermember_id",referencedColumnName = "id")
    private Member sendermember;

    @ManyToOne
    @JoinColumn(name = "recievermember_id",referencedColumnName = "id")
    private Member recievermember;


    @OneToOne(mappedBy = "tasks")
    @JsonIgnore
    private RateAndFeedback rateAndFeedback;

}

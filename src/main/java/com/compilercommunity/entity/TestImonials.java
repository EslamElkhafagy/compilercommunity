package com.compilercommunity.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;


/**
 * this for users feedback on socailMedia
 * */
@Entity
@Data
@Table(name = "testimonials")
public class TestImonials {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String title;

    private String image;

    private String description;

    private String url;

    private Date creationDate;



}

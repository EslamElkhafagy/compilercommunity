package com.compilercommunity.utils;

import java.util.*;

public class Main {


    static String input="represented which group classes store collection Vector architecture to store Java";
    public static HashMap<String, HashSet<String>> loadData(){

        HashMap<String, HashSet<String>> posts = new HashMap<>();
        String s1="The Collection in Java is a framework that provides an architecture to store and manipulate the group of objects. ";
        HashSet<String> set1= new HashSet<>();
        set1.addAll(Arrays.asList(s1.split(" ")));

        posts.put("s1",set1);
        String s2="java Collection framework provides many  interfaces (Set, List, Queue, Deque) and classes (ArrayList, Vector,  LinkedList, PriorityQueue, HashSet, LinkedHashSet, TreeSet).";
        HashSet<String> set2= new HashSet<>();
        set2.addAll(Arrays.asList(s2.split(" ")));

        posts.put("s2",set2);
        String s3="Any group of individual objects which are represented as a single unit is known as the collection of the objects. In Java, a separate framework ";

        HashSet<String> set3= new HashSet<>();
        set3.addAll(Arrays.asList(s3.split(" ")));

        posts.put("s3",set3);


        return posts;
    }



    public static void main(String[] args) {

        String[] keyword = input.split(" ");

        HashMap<String, HashSet<String>> posts =loadData();

        ArrayList<String> list_id = new ArrayList<String>();
        TreeMap<Integer, String> postscount = new TreeMap<Integer, String>();

        /*
        *  count duplicated word from input at each record in posts " s1 , s2 , s3 "
        * */
        for (Map.Entry<String, HashSet<String>> e : posts.entrySet()) {
            int count = 0;
            for (int i = 0; i < keyword.length; i++) {
                if (e.getValue().contains(keyword[i])) {
                    count++;
                }
            }
            postscount.put(count,e.getKey());
        }


    }





}
